package eliboy.math;


public class PlayDivisionEasy extends PlayDivision {

    @Override
    public int createResult() {
        return random.nextInt(10) + 1;
    }

    @Override
    public int createRightMember() {
        return random.nextInt(10) + 1;
    }

    @Override
    public int createLeftMember() {
        return right_number * result;
    }

    @Override
    public void setBcBounty() {
        bcBounty = 4;
    }

}
