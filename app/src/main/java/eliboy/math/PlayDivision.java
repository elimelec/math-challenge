package eliboy.math;

public abstract class PlayDivision extends PlaySuperClassBasicOperations {

    @Override
    public void refresh() {
        result = createResult();
        right_number = createRightMember();
        left_number = createLeftMember();
        correct_button = createRandomButton();

        createErrors();
        printValueMembers();
        printValueButtons();
    }

    @Override
    public void setLayout() {
        setContentView(R.layout.play_division);
    }

}

	

