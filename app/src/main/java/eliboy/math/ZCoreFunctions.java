package eliboy.math;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

/**
 * This class take care of everything
 * cool, huh?
 */

public abstract class ZCoreFunctions extends Activity {

    private int bc;

    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor sharedPreferencesEditor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sharedPreferences = getSharedPreferences(ZFinals.SHARED_PREFS_NAME, Context.MODE_PRIVATE);
        sharedPreferencesEditor = sharedPreferences.edit();
    }

    void bcAddBc(int bsToAdd) {
        bc = bcGetBc() + bsToAdd;
        bcSaveBc(bc);
    }

    void bcRemoveBc(int bcToRemove) {
        bc = bcGetBc() - bcToRemove;
        if (bc <= 0) bc = 0;
        bcSaveBc(bc);
    }

    int bcGetBc() {
        return sharedPreferences.getInt(ZFinals.SCORES_TOTAL_BC, 0);
    }

    private void bcSaveBc(int bcToPut) {
        if (bcToPut <= 0) bcToPut = 0;
        sharedPreferencesEditor.putInt(ZFinals.SCORES_TOTAL_BC, bcToPut);
        sharedPreferencesEditor.commit();
    }

    int settingsGetAnimationType() {
        return sharedPreferences.getInt(ZFinals.SETTINGS_ANIMATION, ZFinals.DEFAULT_ANIMATION_TYPE);
    }

    void settingsSetAnimationType(int type) {
        sharedPreferencesEditor.putInt(ZFinals.SETTINGS_ANIMATION, type);//1 horizontal, 2 vertical
        sharedPreferencesEditor.commit();
    }

    void animationHelper(View v, int id) {
        Animation animation = AnimationUtils.loadAnimation(this, id);
        v.startAnimation(animation);
    }

    //For games
    //Game and check + others
    void gameAnimateIN(View layoutToAnim, int animId) {
        animationHelper(layoutToAnim, animId);
    }

    void gameAnimateOUT(View layoutToAnim, int animId) {
        animationHelper(layoutToAnim, animId);
    }
}
