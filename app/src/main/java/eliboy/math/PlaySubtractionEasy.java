package eliboy.math;


public class PlaySubtractionEasy extends PlaySubtraction {


    @Override
    public int createLeftMember() {
        return random.nextInt(15) + 6;
    }

    @Override
    public int createRightMember() {
        do {
            return random.nextInt(10) + 1;
        } while (left_number < right_number);
    }

    @Override
    public void setBcBounty() {
        bcBounty = 2;
    }

}
