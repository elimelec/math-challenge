package eliboy.math;

public abstract class PlayAddition extends PlaySuperClassBasicOperations {

    @Override
    public void setLayout() {
        setContentView(R.layout.play_addition);
    }

    @Override
    public int createResult() {
        return left_number + right_number;
    }

}
