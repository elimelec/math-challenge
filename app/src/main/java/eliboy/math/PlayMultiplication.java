package eliboy.math;

public abstract class PlayMultiplication extends PlaySuperClassBasicOperations {

    @Override
    public void setLayout() {
        setContentView(R.layout.play_multiplication);
    }

    @Override
    public int createResult() {
        return left_number * right_number;
    }


}

	

