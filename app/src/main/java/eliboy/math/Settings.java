package eliboy.math;


import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.RadioButton;


public class Settings extends ZCoreFunctions {

    private int animationTypeTemp;

    private boolean animationHasChanged = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings);


        RadioButton animationsHorizontal = (RadioButton) findViewById(R.id.settings_animations_horizontal);
        RadioButton animationsVertical = (RadioButton) findViewById(R.id.settings_animations_vertical);
        RadioButton animationsNoAnimations = (RadioButton) findViewById(R.id.settings_animations_no_animations);


        switch (settingsGetAnimationType()) {
            case 1:
                animationsHorizontal.setChecked(true);
                animationsVertical.setChecked(false);
                animationsNoAnimations.setChecked(false);
                break;
            case 2:
                animationsHorizontal.setChecked(false);
                animationsVertical.setChecked(true);
                animationsNoAnimations.setChecked(false);
                break;
            case 0:
                animationsHorizontal.setChecked(false);
                animationsVertical.setChecked(false);
                animationsNoAnimations.setChecked(true);
                break;
        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();

        if (!animationHasChanged) {
            animationTypeTemp = settingsGetAnimationType();
        }

        settingsSetAnimationType(animationTypeTemp);


        this.finish();
        Intent intent = new Intent(this, Main.class);
        startActivity(intent);


    }

    public void onRadioButtonClicked(View v) {
        // Is the button now checked?
        boolean checked = ((RadioButton) v).isChecked();

        // Check which radio button was clicked
        switch (v.getId()) {
            case R.id.settings_animations_horizontal:
                if (checked) {
                    animationTypeTemp = 1;
                    animationHasChanged = true;
                }


                break;
            case R.id.settings_animations_vertical:
                if (checked) {
                    animationTypeTemp = 2;
                    animationHasChanged = true;
                }
                break;
            case R.id.settings_animations_no_animations:
                if (checked) {
                    animationTypeTemp = 0;
                    animationHasChanged = true;
                }
                break;
        }
    }


}
