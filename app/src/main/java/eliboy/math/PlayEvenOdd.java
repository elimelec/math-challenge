package eliboy.math;

import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.Random;

public class PlayEvenOdd extends ZCoreFunctions {


    private int number;
    private boolean even = false;
    private TextView textViewNumber;

    private int animDelayTime = 0;
    private int animListAppear3;
    private int animListDisappear3;

    private Button buttonEven;
    private Button buttonOdd;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.play_even_odd);

        buttonEven = (Button) findViewById(R.id.button_1);
        buttonOdd = (Button) findViewById(R.id.button_2);


        switch (settingsGetAnimationType()) {
            case 1:
                animListAppear3 = R.anim.horizontal_list_appear_3;
                animListDisappear3 = R.anim.horizontal_list_disappear_3;
                animDelayTime = 500;
                break;
            case 2:
                animListAppear3 = R.anim.vertical_list_appear_3;
                animListDisappear3 = R.anim.vertical_list_disappear_3;
                animDelayTime = 500;
                break;
            case 0:
                animListAppear3 = R.anim.empty;
                animListDisappear3 = R.anim.empty;
                animDelayTime = 300;
                break;
        }

        textViewNumber = (TextView) findViewById(R.id.layout_exercise_area);

        refresh();
    }


    private void refresh() {
        Random random = new Random();
        number = random.nextInt(2000) + 1;

        even = number % 2 == 0;

        textViewNumber.setText(Integer.toString(number));

    }


    public void checkAnswer(View v) {
        switch (v.getId()) {
            case R.id.button_1: // even
                if (even) {
                    bcAddBc(1);
                } else {
                    bcRemoveBc(1);
                }


                break;
            case R.id.button_2: // odd
                if (!even) {
                    bcAddBc(1);
                } else {
                    bcRemoveBc(1);
                }


                break;
        }

        if (even) {
            buttonEven.setBackgroundResource(R.drawable.button_correct);
            buttonOdd.setBackgroundResource(R.drawable.button_incorrect);
        } else {
            buttonEven.setBackgroundResource(R.drawable.button_incorrect);
            buttonOdd.setBackgroundResource(R.drawable.button_correct);
        }
        gameAnimateOUT(textViewNumber, animListDisappear3);
        new Handler().postDelayed(
                new Runnable() {
                    public void run() {
                        buttonEven.setBackgroundResource(R.drawable.button_neutral);
                        buttonOdd.setBackgroundResource(R.drawable.button_neutral);
                        refresh();
                        gameAnimateIN(textViewNumber, animListAppear3);
                    }
                }, animDelayTime);


    }


}
