package eliboy.math;


public class PlayAdditionMedium extends PlayAddition {

    @Override
    public int createLeftMember() {
        return random.nextInt(20) + 11;
    }

    @Override
    public int createRightMember() {
        return random.nextInt(20) + 11;
    }

    @Override
    public void setBcBounty() {
        bcBounty = 2;
    }

}
