package eliboy.math;


public class PlayAdditionEasy extends PlayAddition {

    @Override
    public int createLeftMember() {
        return random.nextInt(10) + 1;
    }

    @Override
    public int createRightMember() {
        return random.nextInt(10) + 1;
    }

    @Override
    public void setBcBounty() {
        bcBounty = 1;
    }

}
