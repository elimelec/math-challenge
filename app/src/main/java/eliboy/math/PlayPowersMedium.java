package eliboy.math;

import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.Random;

public class PlayPowersMedium extends ZCoreFunctions {

    private View layoutExerciseArea;
    private int animDelayTime = 0;
    private int animListAppear3;
    private int animListDisappear3;


    private TextView baseM;
    private TextView exponentM;
    private Button button_1;
    private Button button_2;
    private Button button_3;
    private int correct_button;
    private int error_1;
    private int error_2;
    private int base;
    private int exponent;
    private double result;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.play_powers);


        switch (settingsGetAnimationType()) {
            case 1:
                animListAppear3 = R.anim.horizontal_list_appear_3;
                animListDisappear3 = R.anim.horizontal_list_disappear_3;
                animDelayTime = 500;
                break;
            case 2:
                animListAppear3 = R.anim.vertical_list_appear_3;
                animListDisappear3 = R.anim.vertical_list_disappear_3;
                animDelayTime = 500;
                break;
            case 0:
                animListAppear3 = R.anim.empty;
                animListDisappear3 = R.anim.empty;
                animDelayTime = 300;
                break;
        }

        layoutExerciseArea = findViewById(R.id.layout_exercise_area);


        refresh();

    }


    
    
    /*
        * This method refresh the values giving a
   	 * new exercise every time a user finish one
   	 * */

    private void refresh() {
        Random random = new Random();

   			/*
   			 * We randomize the base first.
   			 * Then, based on the value of the base, we generate
   			 * the exponent and the errors :)
   			 */
        base = random.nextInt(11);
   			/*
   			 * If base is 0, then the result always will be 0.
   			 * The exponent can have any value, but here is limited to 1 -> 20
   			 * The value of the exponent can not be 0
   			 */
        if (base == 0) {
            exponent = random.nextInt(20) + 1;
        }
   			/*
   			 * If base is 1, then the result always will be 1.
   			 * The exponent can have any value, but here is limited to 1 -> 20
   			 */
        else if (base == 1) {
            exponent = random.nextInt(21);
        }
   			/*
   			 * For base = 2, we can choose a exponent up to 10
   			 */
        else if (base == 2) {
            exponent = random.nextInt(11);
        }
   			
   			/*
   			 * For base = 3, we can chose a exponent up to 6
   			 */
        else if (base == 3) {
            exponent = random.nextInt(7);
        }
   			/*
   			 * For base = 4, we can chose a exponent up to 5
   			 */
        else if (base == 4) {
            exponent = random.nextInt(6);

        }
   			/*
   			 * For base = 5, we can chose a exponent up to 4
   			 */
        else if (base == 5) {
            exponent = random.nextInt(5);
        }
   			
   			/*
   			 * For base = 6, 7, 8 or 9, we can chose a exponent up to 3
   			 * Math is hard...
   			 */
        else if (base == 6 || base == 7 || base == 8 || base == 9) {
            exponent = random.nextInt(4);
        }
   			/*
   			 * If base is 10, then the result always will be 1 followed by zeros.
   			 * The exponent can have any value, but here is limited to 0 -> 4
   			 */
        else if (base == 10) {
            exponent = random.nextInt(5);
        }

        // result
        result = Math.pow(base, exponent);
        //wrong answers
        do {
            error_1 = (int) (result + random.nextInt(11) - 5);
        }
        while (error_1 == 0 || error_1 <= 1 || error_1 == result);

        do {
            error_2 = (int) (result + random.nextInt(11) - 5);
        }
        while (error_2 == 0 || error_2 == error_1 || error_2 <= 1 || error_2 == result);
   			
   			
   			
   			
   			/*
   			 * This randomize the answer
   			 */
        correct_button = random.nextInt(3) + 1;
   			
   			/*
   			 * Update layout
   			 */


        baseM = (TextView) findViewById(R.id.base);
        baseM.setText(Integer.toString(base));

        exponentM = (TextView) findViewById(R.id.exponent);
        exponentM.setText(Integer.toString(exponent));

        button_1 = (Button) findViewById(R.id.button_1);
        button_2 = (Button) findViewById(R.id.button_2);
        button_3 = (Button) findViewById(R.id.button_3);

        if (correct_button == 1) {
            button_1.setText(Integer.toString((int) result));
            button_2.setText(Integer.toString(error_1));
            button_3.setText(Integer.toString(error_2));
        } else if (correct_button == 2) {
            button_2.setText(Integer.toString((int) result));
            button_1.setText(Integer.toString(error_1));
            button_3.setText(Integer.toString(error_2));
        } else if (correct_button == 3) {
            button_3.setText(Integer.toString((int) result));
            button_1.setText(Integer.toString(error_1));
            button_2.setText(Integer.toString(error_2));
        }

    }
   	
   	/*
   	 * This checks if the chosen answer is correct
   	 */

    public void checkAnswer(View v) {
        switch (v.getId()) {
            case R.id.button_1:
                if (correct_button == 1) {
                    bcAddBc(3);
                } else {
                    bcRemoveBc(3);
                }
                break;
            case R.id.button_2:
                if (correct_button == 2) {
                    bcAddBc(3);
                } else {
                    bcRemoveBc(3);
                }
                break;
            case R.id.button_3:
                if (correct_button == 3) {
                    bcAddBc(3);
                } else {
                    bcRemoveBc(3);
                }
                break;
        }


        button_1.setBackgroundResource(R.drawable.button_incorrect);
        button_2.setBackgroundResource(R.drawable.button_incorrect);
        button_3.setBackgroundResource(R.drawable.button_incorrect);

        switch (correct_button) {
            case 1:
                button_1.setBackgroundResource(R.drawable.button_correct);
                break;
            case 2:
                button_2.setBackgroundResource(R.drawable.button_correct);
                break;
            case 3:
                button_3.setBackgroundResource(R.drawable.button_correct);
                break;
        }

        gameAnimateOUT(layoutExerciseArea, animListDisappear3);
        new Handler().postDelayed(
                new Runnable() {
                    public void run() {
                        button_1.setBackgroundResource(R.drawable.button_neutral);
                        button_2.setBackgroundResource(R.drawable.button_neutral);
                        button_3.setBackgroundResource(R.drawable.button_neutral);
                        refresh();
                        gameAnimateIN(layoutExerciseArea, animListAppear3);
                    }
                }, animDelayTime);
    }


}
